import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MainList from "./src/container/MainList/MainList";

export default class App extends React.Component {

  render() {
    return (
      <MainList/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
