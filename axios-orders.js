import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://www.reddit.com/r/pics.json?count=25&'
});


export default instance;