import React, {Component} from "react";
import {connect} from "react-redux";
import Item from "../../component/Item/Item";
import {getLastElement, getPosts} from "../../store/action";
import {ActivityIndicator, FlatList, View} from "react-native";

class MainList extends Component {
    componentDidMount() {
        this.props.getPosts();
    }

    render() {
        console.log(this.props.lastId);
        let myList = [];
        for (let i in this.props.posts) {
            console.log(i);
            let onePost = this.props.posts[i].data;
            myList.push({
                key: onePost.id, postImg: {uri: onePost.thumbnail}, title: onePost.title,
            })
        }
        return (

            <View>
                <FlatList /*Что то не получилось выполнить догрузку onEndReached={() => this.props.getPosts(this.props.lastId)}*/
                    data={myList}
                          renderItem={(info) => {
                              return <Item
                                  postImg={info.item.postImg}
                                  title={info.item.title}
                              />
                          }}/>


                <ActivityIndicator size="large" color="#0000ff"/>
            </View>)

    }
}
const mapStateToProps = state => {
    return {
        posts: state.posts,
        loading: state.loading,
        error: state.error,
        lastId: state.lastId

    }
};
const mapDispatchToProps = dispatch => {
    return {
        getPosts: () => dispatch(getPosts()),
        getLastElement: () => dispatch(getLastElement())

    }
};
export default connect(mapStateToProps, mapDispatchToProps)(MainList);