import axios from "../../axios-orders";

export const DATA_REQUEST = 'DATA_REQUEST';
export const DATA_SUCCESS = 'DATA_SUCCESS';
export const DATA_ERROR = 'DATA_ERROR';
export const LAST_MESSAGE = 'LAST_MESSAGE';


export const dataRequest = () => {
    return {type: DATA_REQUEST}
};
export const dataSuccess = (data) => {
    return {type: DATA_SUCCESS, data}
};

export const dataError = () => {
    return {type: DATA_ERROR}
};
export const getLastElement = (lastId) => {
    return {type: LAST_MESSAGE, lastId}
};

export const getPosts = (lastElemet = '') => {

    return (dispatch, getState) => {

        dispatch(dataRequest());
        axios.get(lastElemet).then(response => {
            console.log(lastElemet);
            dispatch(getLastElement(response.data.data.after));
            dispatch(dataSuccess(response.data.data.children));

        }, error => {
            dispatch(dataError())
        })
    }
};



