import * as actionType from "./action";
const initialState = {
        posts: {},
        loading: false,
        error: false,
        lastId: ''
    }
;
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.DATA_REQUEST: {
            return {...state, posts: {...state.posts}, loading: true, error: false}
        }
        case actionType.DATA_ERROR: {

            return {...state, posts: {...state.posts}, loading: false, error: true}
        }
        case actionType.DATA_SUCCESS: {
            let postList=Object.assign(state.posts,action.data);
            return {...state, posts: action.data, loading: false}

        }
        case actionType.LAST_MESSAGE: {
            return {...state, posts: {...state.posts}, lastId: action.lastId}
        }

        default:
            return state;
    }


};
export default reducer;